<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>I have made some progress with the syntax parsing function, and this will outline my current approach to the problem.</p>
    
    <p>The following is a BNF description of my approach.</p>
    
    <p>expr -> comp</p>
    <p>comp -> "m"oper | oper | "m""("expr")"</p>
    <p>oper -> term OP expr | term</p>
    <p>term -> ID</p>
    
    <p>"m" denotes the character representing my complement operator.</p>
    <p>"(" and ")" represent parentheses.</p>
    <p>My operators which could appear in place of "OP" all require two arguments, unlike complement. I am using "u", "n", and "f" to denote union, intersection, and difference respectively in my backend. There is no space between them in my code, but it helps readability.</p>
    <p>My sets are represented as "ID", with possible values of "A", "B", "C", "D", and "E".</p>
    
    <p>I definitely need a second opinion on this, but it is enough to design the rest of my functions around.</p>
    
</div>