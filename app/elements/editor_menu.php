<nav class="navbar navbar-default navbar-fixed-top" style="margin-top:50px">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <!-- Creates the dropdown menus and the modal window for the editor page -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">File
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#" id="new"><i class="glyphicon glyphicon-file"></i> New</a>
                        </li>
                        <li>
                            <a href="processor.php?action=save_schematic&id=<?php echo(!empty($_GET['id']) ? $_GET['id'] : ''); ?>" id="save">
                                <i class="glyphicon glyphicon-save"></i> Save
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="#" id="import" data-toggle="modal" data-target="#import-modal">
                                <i class="glyphicon glyphicon-import"></i> Import
                            </a>
                        </li>
                        <li>
                            <a href="#" id="export"><i class="glyphicon glyphicon-export"></i> Export</a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="#" id="print" onclick="window.print();">
                                <i class="glyphicon glyphicon-print"></i> Print
                            </a>
                        </li>
                        <li>
                            <a href="?page=browse">Browse Schematics</a>
                        </li>
                        <li>
                            <a href="#" id="help" data-toggle="modal" data-target="#help-modal">Help</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Edit
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#" id="undo"><i class="fa fa-undo"></i> Undo</a>
                        </li>
                        <li>
                            <a href="#" id="redo"><i class="fa fa-repeat"></i> Redo</a>
                        </li>
                    </ul>
                </li>
                <!-- Trigger the modal with a button -->
                <li data-toggle="modal" data-target="#inputModal">
                    <a href="#" role="button">Input Selection</a>
                </li>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" id="setOutputEditor" aria-label="..." placeholder="{}" disabled>
                    </div>
                </form>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav><!-- Modal -->
<?php $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']; ?>
<div id="inputModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Input Selection</h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control" id="setOutputModal" aria-label="..." placeholder="{}" disabled>
                <div class="modal-body row">
                    <div class="col-md-6">
                        <strong>Toggle set elements below</strong>
                        <div class="pad">Universal Set</div>
                        <div class="btn-group" role="group" aria-label="...">
                            <?php foreach($letters as $letter) { ?>
                                <button type="button" class="btn btn-default" disabled><?php echo $letter; ?></button>
                            <?php } ?>
                        </div>
                        <div class="pad">Set A</div>
                        <div class="btn-group" data-toggle="buttons" id="setA">
                            <?php foreach($letters as $letter) { ?>
                                <label class="btn btn-primary">
                                    <input type="checkbox" id="A<?php echo $letter; ?>Toggle"><?php echo $letter; ?>
                                </label>
                            <?php } ?>
                        </div>
                        <div class="pad">Set B</div>
                        <div class="btn-group" data-toggle="buttons" id="setB">
                            <?php foreach($letters as $letter) { ?>
                                <label class="btn btn-primary">
                                    <input type="checkbox" id="B<?php echo $letter; ?>Toggle"><?php echo $letter; ?>
                                </label>
                            <?php } ?>
                        </div>
                        <div class="pad">Set C</div>
                        <div class="btn-group" data-toggle="buttons" id="setC">
                            <?php foreach($letters as $letter) { ?>
                                <label class="btn btn-primary">
                                    <input type="checkbox" id="C<?php echo $letter; ?>Toggle"><?php echo $letter; ?>
                                </label>
                            <?php } ?>
                        </div>
                        <div class="pad">Set D</div>
                        <div class="btn-group" data-toggle="buttons" id="setD">
                            <?php foreach($letters as $letter) { ?>
                                <label class="btn btn-primary">
                                    <input type="checkbox" id="D<?php echo $letter; ?>Toggle"><?php echo $letter; ?>
                                </label>
                            <?php } ?>
                        </div>
                        <div class="pad">Set E</div>
                        <div class="btn-group" data-toggle="buttons" id="setE">
                            <?php foreach($letters as $letter) { ?>
                                <label class="btn btn-primary">
                                    <input type="checkbox" id="E<?php echo $letter; ?>Toggle"><?php echo $letter; ?>
                                </label>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <strong>Enter notation below</strong>
                        <div class="pad">Controls</div>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" id="backspace">Backspace</button>
                                <button class="btn btn-default" type="button" id="clear">Clear</button>
                                <button class="btn btn-default" type="button" id="enter">Enter</button>
                            </div>
                        </div>
                        <div class="pad">Sets</div>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" id="a">A</button>
                                <button class="btn btn-default" type="button" id="b">B</button>
                                <button class="btn btn-default" type="button" id="c">C</button>
                                <button class="btn btn-default" type="button" id="d">D</button>
                                <button class="btn btn-default" type="button" id="e">E</button>
                            </div>
                        </div>
                        <div class="pad">Operators</div>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" id="union">⋃</button>
                                <button class="btn btn-default" type="button" id="intersection">∩</button>
                                <button class="btn btn-default" type="button" id="difference">\</button>
                                <button class="btn btn-default" type="button" id="complement">￢</button>
                                <button class="btn btn-default" type="button" id="lparen">(</button>
                                <button class="btn btn-default" type="button" id="rparen">)</button>
                            </div>
                        </div>
                        <div class="pad">Randomize String</div>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" id="randomize">Click for Similar String</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>