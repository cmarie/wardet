<div class="container">

    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>Ethan Ward's Set Notation Visualizer</h1>
            <p></p>
            <p class="lead">This project seeks to create a tool to help visualize set notation</p>
            <p>Clicking on the editor tab will take you to the visualizer</p>
            <p>You can create an account to save some usage statistics and visualizations on a server, but this is not required</p>
            <p>In the editor, clicking the "Input Selection" button will display the input controls</p>
            <img src="app/webroot/img/output.png" height="531" width="678"/>
            <ul class="list-unstyled">
                <li>Bootstrap v3.3.7</li>
                <li>jQuery v1.11.1</li>
            </ul>
        </div>
    </div>
    <!-- /.row -->

</div>