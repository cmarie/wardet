<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>This past week saw the completion of the postfix evaluation and the creation of my tree structure with data. I am now working on converting the tree into JSON needed for my output, so figuring out exactly how to do that is getting interesting. Determining coordinates for the output will also be challenging, and I know that to accomplish that I will require knowing the height of each node on the tree.</p>
    
    <p>Saving to the database was not functional before, and that has been fixed. Users can now save schematics to the server or to their local machine.</p>
    
    <p>Basic randomization has been added, fulfilling another one of my requirements. This keeps the basic structure of an existing input and then randomizes the sets and binary operators.</p>
    
    <p>The trees which I was so fervently pursuing might not actually be needed now. Even if they are not used, the concepts behind them have been very beneficial to planning and understanding what I need for my data.</p>
    
    <p>I present a week from Thursday, so that will mark the end of coding and the beginning of documentation. Currently, there is essentially no documentation.</p>
    
    <p>With generating JSON being the only foreseeable roadblock left, there is an odd sense of relief and panic now taking form.</p>

</div>