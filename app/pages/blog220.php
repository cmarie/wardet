<div class="container">

        <h2>Ethan Ward's Project Blog</h2>

        <p>I have been given clearance to use the SimCirJS library, but this comes with an expanded project definition to compensate for the capabilities of the library. Time will tell if I have gotten myself in trouble by pursuing this. I will say that I am lucky in a way. As I have spent the last few days trying to learn more about this library, a number of functions and features have been made with it. Certainly not a smart course of action, but based on how my meeting went on Friday, I felt I could get away with spending the time on it, plus I wasn’t entirely sure how else to figure out the capabilities of the library other than working with it. Looking at the source code alone has not provided all the answers needed, and I will readily admit that there are parts of it that are way over my head.</p>

        <p>Taking this path has a number of risks. Most present in my mind at the moment is the fact that I will likely have no resources to consult for this library. While I have a history of pursuing ideas that take me into similar unknown territory, the stakes are higher with the capstone. Having only received the new definition last night, I do not fully know how the new requirements translate into design. A meeting today should clarify much of what exactly is required with the expanded definition. The project definition page will be changing accordingly after this update is posted.</p>

        <p>Beyond the new project definition, minimizing duplicate code across the site has been more or less finished, and the results of that should be uploaded in the next day or so.</p>

    </div>