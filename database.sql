CREATE TABLE `schematics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `data` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `password` char(42) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `count_a` int(11) DEFAULT '0',
  `count_b` int(11) DEFAULT '0',
  `count_c` int(11) DEFAULT '0',
  `count_d` int(11) DEFAULT '0',
  `count_e` int(11) DEFAULT '0',
  `count_union` int(11) DEFAULT '0',
  `count_inter` int(11) DEFAULT '0',
  `count_diff` int(11) DEFAULT '0',
  `count_comp` int(11) DEFAULT '0',
  `max_depth` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;