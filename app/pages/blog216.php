<div class="container">

        <h2>Ethan Ward's Project Blog</h2>

        <p>The week has been productive so far. Two iterations of the editor’s tools menu have been made, and I think I will be pursuing the more simple of them. Initially, the goal was to have an overlay appear from the left side of the screen, and I had managed to get an overlay that was toggled with buttons. It worked well enough, but I got input from others that suggested it was harder to read than it needed to be. Combined with the hassle of coding it and laying out elements on it, I looked for an alternative. Reusing the navbar code for a menu seemed easy enough.</p>

        <img src="app/webroot/img/menuV1.png" height="586" width="661"/>


        <img src="app/webroot/img/menuV2.png" height="359" width="941"/>

        <p></p>
        <p></p>
        <p>With that taken care of, I have moved onto other parts of the problem. For the graphical objects, my plan was initially to just size the grid and images so that the existing wires in the images lined up with the grid. Ultimately, I warped the images I had been using without much actual success. I turned to looking for a method to just snap to anchors on the object instead. That is still underway, but I am hoping to have it finished by the weekend. I am also looking into other sets of images to use since the set I am currently using does not have all of the gates required.</p>

        <p>For the site itself, I have found myself constantly forgetting little things in pages like including libraries or tags, so I will likely try to streamline the process for making pages, so php may creep back into the site in the coming days.</p>

    </div>