<div class="container">
    <?php getFLash(); ?>
    <?php if(!loggedIn()){ ?>
        <h2>Login</h2>
        <form action="processor.php?action=login" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="data[users][email]">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="data[users][password]">
            </div>
            <button type="submit" class="btn btn-default">Login</button>
            <a href="?page=register">No Account? Sign up!</a>
        </form>
    <?php }else{ ?>
        <h2>You're logged in!</h2>
        <?php debug(user()); ?>
    <?php } ?>
</div>