<div class="container">

        <h2>Ethan Ward's Project Blog</h2>

        <p>This weekend was a productive one with two major focuses. First, many small corrections were made to portions of the site, like removing all references to the database from pages and changing the navbar to stay fixed to the top of the page.</p>

        <p>The second focus and bulk of the work done was on the circuit editor page itself. It now has a navbar that does not cover the canvas. The workspace has been expanded vastly, producing something of a usable size. Scaling of the grid and the elements to be placed on it still needs to be done, but that isn’t as crucial as functionality.</p>

        <p>Line drawing now snaps to the grid, though this isn’t visible at the moment as my attempts to detect what element the mouse was over when clicking is not working, so the conditions for line drawing are not being met in the code. Rotation and scaling controls have been removed from the current objects, ensuring that they stay aligned to the grid for the time being. They have been set up with the ability to be rotated in 90 degree increments in the event the ability can be reintroduced in the future.</p>

        <p>The final major addition was a button for a menu overlay. The menu currently has no content in it, and the button itself needs to be changed. Currently, it sits just below the navbar, but does not stay on the screen if the user scrolls elsewhere in the workspace. My goal is to have this stay on the screen at all times - it wouldn’t be of much use otherwise. One potential solution would be adding it to the navbar since that is already a constant presence on screen.</p>

        <p>This week will hopefully bring a solution to that, a completed menu layout, and a finalized data structure.</p>

    </div>