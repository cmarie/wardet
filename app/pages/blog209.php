<div class="container">

        <h2>Ethan Ward's Project Blog</h2>

        <p>Again, major changes have been made to the website. CakePHP has been abandoned due to the overcomplexity it brings to the project. Twitter Bootstrap is still in use however. All the content from V1 was taken and reused, so all is good there. I’m mortified by how easy this new solution was to implement, taking 40 minutes to essentially duplicate the sum of 18 hours of work with zero errors.</p>

        <p>The database problem is being solved by also abandoning that as well. After speaking with Dr. McVey, it became apparent that my approach to that was both excessive and probably wrong given what I was likely to end up working with for data.</p>

        <p>As for the data, it was made clear to me in that same meeting that I was somewhat on the right track with some things, but my general understanding of what was needed for a data structure was a bit off. I was rambling a bit in the last progress update on what kind of structure I might need and the answer is now very clear - a directed graph. Figuring out how to implement that is the next step there.</p>

        <p>For the workspace, I found something called Fabric.js that had examples of snapping images to a grid. I want to pursue this as it was easy to interact with in the demos they had available, and it has a completely open license. I hope to have a preliminary test up by the end of the day.</p>

        <p>I have also created a Gantt Chart outlining the the ideal course of development. That will be included in a separate link.</p>

        <p>On an introspective note, I am finding a number of Fred Brooks’ points becoming more tangible, after all, I have just thrown my first build away. Plans never survive contact with the enemy and all that.</p>

    </div>