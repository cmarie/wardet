<div class="container">
    <h2>Schematic Browser</h2>
    <?php foreach(db()->find('all', 'schematics') as $schematic) { ?>
        <ul class="list-group">
            <li class="list-group-item">
                <a href="processor.php?action=delete_schematic&id=<?php echo $schematic['id']; ?>" class="btn btn-sm btn-danger">
                    <i class="glyphicon glyphicon-trash"></i>
                </a>

                <a href="?page=editor&id=<?php echo $schematic['id']; ?>">Schematic <?php echo $schematic['created']; ?> by <?php echo db()->read('users', $schematic['user_id'])['email']; ?></a>
            </li>
        </ul>
    <?php } ?>
</div>