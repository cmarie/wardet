<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>Yesterday saw the addition of a tree structure for javascript adapted from Cho Kim. Trees are going to play a large role in what I need to do still, so having the ability to use them makes my life significantly easier. Kim notes that trees are one of the most commonly used structures in web development, reinforcing my confusion over the lack of any apparent tree structures in the language. Perhaps this was/is just my unfamiliarity with the language displaying itself.</p>
    
    <p>I also began digging deeper into the Simcir library for designing components. I discovered a vastly easier approach to creating the visuals needed for everything. The library has the ability to embed larger circuits into single visual components. These components can have all of the i/o points for connections that I need, but these points can be labelled, something I wasn’t finding easily doable with the stock parts. Designing these “devices” allows me to then display information about the part more easily, since I can just print a char in a label rather than recreate the char through html/js canvas commands. This also allows more information to be displayed for a part with multiple inputs and outputs, so these labels will generally convey more than just using a symbol. The set devices have been completed, but the operators are still in the works.</p>
    
    <p>I feel that these are significant milestones, so the final result is suddenly feeling more achievable than it was a few days ago.</p>

</div>