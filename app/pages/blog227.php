<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>The weekend was productive, though perhaps not as productive as it should have been as I appear to be getting ill. Following the updates to the ui adding set notation inputs and a method of selecting the contents of sets, the backend work has begun. While none of the changes have gone live as of when this was written, the event handlers have been written for the set notation buttons, so users will have the ability to punch in their input and see it appear in the input box. The parse function is not yet in place, but will be by the end of the week. My current plan is to have the validity of the input checked only at the time of clicking the equivalent of the enter button, rather than on the fly.</p>
    
    <p>As I am writing this, a potential issue occurred to me. The textbox that the input notation will appear in is only so large, so I may need something like a mouseover tooltip to give the full length of the contents if the user makes the input too large. The field is not enabled, so the user could not click into the box to arrow through the input.</p>
    
    <p>Formatting fixes for the toolbar and dropdown are also underway. All of these updates should go live together.</p>
    
</div>