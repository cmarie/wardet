<?php
    include "core/globals.php";
    header('Content-Type: application/json');
    $action = @$_GET['action'];
    switch($action) {
        case 'get_schematic': //pull saved schematic from database
            $schematic = $db->read('schematics', $_GET['id']);
            if($schematic) {
                echo $schematic['data'];
            } else {
                echo json_encode(['code' => 620, 'msg' => 'Schematic not found']);
            }
            break;
        case 'delete_schematic': //delete schematic from database
            $schematic = $db->read('schematics', $_GET['id']);
            if($schematic) {
                if($db->delete('schematics', $_GET['id'])) {
                    setFlash('Your schematic has been deleted successfully', 'success');
                }
            } else {
                setFlash('The schematic you\'re trying to delete does not exist', 'danger');
            }
            redirect('browse');
            break;
        case 'save_schematic': //save a schematic to the database
            $data['schematics']['data'] = json_encode($_POST);
            if(!empty($_GET['id'])) {
                $data['schematics']['id'] = $_GET['id'];
                if($db->update($data)) {
                    echo json_encode(['code' => 200, 'msg' => 'Your schematic has been saved successfully']);
                }
            } else {
                $id = $db->insert($data);
                if($id) {
                    echo json_encode([
                        'code' => 200,
                        'id' => $id,
                        'msg' => 'Your schematic has been saved successfully'
                    ]);
                }
            }
            break;
        case 'login': //user login
            $check = $db->find('first', 'users', [
                'email' => $data['users']['email'],
                'password' => $data['users']['password']
            ]);
            if($check) {
                $_SESSION['User'] = $check;
            } else {
                setFlash('Email and/or password not found', 'danger');
            }
            redirect('login');
            break;
        case 'register': //create new user
            $check = $db->find('first', 'users', [
                'email' => $data['users']['email']
            ]);
            if($check) {
                setFlash('Email is already registered', 'danger');
                redirect('register');
            } else {
                if($db->insert($data)) {
                    setFlash('Your account has been created. Please login.', 'success');
                    redirect('login');
                }
            }
            break;
        case 'logout': //logout
            session_destroy();
            redirect('login');
            break;
        case 'save_stats': //update user stats
            if($db->update($data))
            {
                echo json_encode(['code' => 200, 'msg' => 'Success']);
            }
            else
            {
                echo json_encode(['code' => 620, 'msg' => 'Error saving stats.']);
            }
            break;
        case 'get_stats': //get user stats from database, currently unused
            $user = $db->read($data['id']);
            if($user)
            {
                echo json_encode(['code' => 200, 'data' => $user]);
            }
            else
            {
                echo json_encode(['code' => 620, 'msg' => 'User not found']);
            }
            break;
        default: //request error
            echo json_encode([
                'code' => 500,
                'msg' => 'Invalid Request: Missing Action'
            ]);
    }