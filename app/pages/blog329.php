<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>Work this week has involved rewriting all of the set operators to work with strings rather than arrays since I can still traverse strings like arrays, but some data manipulation was easier with strings (at least with my little experience with javascript).</p>
    
    <p>I am currently modifying the Simcir library for my new parts. I knew modifying this would be difficult, but the majority of my changes so far have been fairly simple. The functions are all there for the operators, but my hopes of simply printing a char onto the set objects will require far more work than I had hoped. The operators only require the functions that draw their symbols to be completed. I plan on using the existing logic gate functions that already work. Creating the drawing functions requires knowing how to use javascript’s built in drawing functions, and my initial attempt to avoid that under the first iteration of my project took me to Fabric.js.</p>

</div>