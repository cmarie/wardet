<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>This week has seen a few major changes and new progress.</p>
    
    <p>The least critical of these changes was the updating of instances of text still calling the project a circuit builder. That wasn’t critical to usability, but better reflects the purpose of the project (so it looks better).</p>
    
    <p>The largest overhaul for the site came on the editor page. The input controls and set selection dropdown have been removed and replaced with a modal dialog containing both. There is a text field displaying the entered notation in the dialog and underlying form so that you can still see the input regardless of whether the controls are open. This change solves a number of problems that the old methods for input were having. First, this solves a problem that the top bar had with smaller screen or window sizes. The modal resizes appropriately to the size of the window. Second, the sets dropdown would close on each click, and attempts to fix that were unsuccessful. I would like to thank Dani for her help trying to fix that, I think I can take what I learned from some of the JQuery plugins and perhaps apply that later depending on how I handle some of the other controls that need to be redone. The other issue with the dropdowns was formatting. I was having issues getting the elements within the dropdown to align the way I wanted. The modal has no issues with that.</p>
    
    <p>I now have access to the college database server now, thanks to Dr. Pankratz, so I can implement user accounts in order to collect statistics and allow for users to save input sets. I have had most of what I needed for that prepared ahead of time.</p>
    
    <p>The last major milestone was the functions that calculate what is in my sets. Currently, I have an issue efficiently determining the state of my set checkboxes, but when that is resolved, I will have the foundation for all other set functions completed. The operators themselves are also complete, so now something like A union B will actually output the union of what was in those two sets. The previously mentioned problem is determining what would be in A and B in that example. I probably could have approached how I handled my checkboxes more intelligently. Currently, I am trying to access groups of them based on their containing div tag id, rather than more explicitly using arrays. This could be changed, but in its current state, it presents another learning moment with JQuery.</p>

</div>