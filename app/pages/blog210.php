<div class="container">

        <h2>Ethan Ward's Project Blog</h2>

        <p>There might not be much to it at the moment, but there is some basic drawing being done with Fabric.js now in the editor. This is extremely rudimentary, combining a few examples into one working unit. From here, adding a controls panel will ideally be simple, but the greater challenge will be learning about how javascript handles events. I have had experience with this in C#, but that doesn’t mean I know the syntax of javascript. I can also now focus on adding the icons, which are easily displayed with Fabric.js. Obviously, the logic and data structures are nowhere to be seen yet, but proof that I can get graphics to work in HTML5 is a great step forward. What is present on the screen currently needs a bit of work: all elements need resizing, labels need consideration, the toolbox area needs creating.</p>
        <p>An example of the testbed workspace and be seen here</p>

        <img src="app/webroot/img/testbed.png" height="603" width="605"/>
    </div>