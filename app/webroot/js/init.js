var app = {
    history: [],
    history_index: -1,
    last: '',

    stringToParse: "",
    stringToDisplay: "",
    terminals: ["A", "B", "C", "D", "E"],
    basicOperators: ["u", "n", "f"],
    compOperator: "m",

    universalSet: ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
    setA: "",
    setB: "",
    setC: "",
    setD: "",
    setE: "",
    treeRoot: null, //not really used
    treeElements: [],
    treeTiers: [],

    ySpacing: 64, //y offset for workspace elements
    xSpacing: 96, //x offset for workspace elements
    corner: 16, //corner arbitrarily set for aesthetics

    //sets up the simcir workspace and the input event handlers
    init: function () {
        app.setupSchematics();
        app.setupEditorButtons();
    },

    //passes json containing the simcir parameters to a function in the library that generates the workspace
    setupEditor: function (options) {
        var el = $('.simcir');
        if (!options) {
            options = [];
        } //json defining workspace
        options = $.extend({
            width: window.innerWidth,
            height: window.innerHeight - 100,
            showToolbox: true,
            toolbox: [
                {type: 'Set A'}, //A
                {type: 'Set B'}, //B
                {type: 'Set C'}, //C
                {type: 'Set D'}, //D
                {type: 'Set E'}, //E
                {type: 'Complement'}, //complement
                {type: 'Union'}, //union
                {type: 'Intersection'}, //intersection
                {type: 'Difference'}  //difference
            ]
        }, options);

        // Override saved width and height with client window, probably redundant at this point
        options.width = window.innerWidth;
        options.height = window.innerHeight - 100;

        simcir.setupSimcir(el, options);
    },

    //event handlers for the php buttons
    setupEditorButtons: function () {
        $('#backspace').on('click', function (e) { //backspace button
            e.preventDefault();
            app.onBack();
        });
        $('#a').on('click', function (e) { //set a button
            e.preventDefault();
            app.onA();
        });
        $('#b').on('click', function (e) { //set b button
            e.preventDefault();
            app.onB();
        });
        $('#c').on('click', function (e) { //set c button
            e.preventDefault();
            app.onC();
        });
        $('#d').on('click', function (e) { //set d button
            e.preventDefault();
            app.onD();
        });
        $('#e').on('click', function (e) { //set e button
            e.preventDefault();
            app.onE();
        });
        $('#enter').on('click', function (e) { //enter button
            e.preventDefault();
            app.onEnter();
        });
        $('#clear').on('click', function (e) { //clear button
            e.preventDefault();
            app.onClear();
        });
        $('#union').on('click', function (e) { //union button
            e.preventDefault();
            app.onUnion();
        });
        $('#intersection').on('click', function (e) { //intersection button
            e.preventDefault();
            app.onIntersection();
        });
        $('#difference').on('click', function (e) { //difference button
            e.preventDefault();
            app.onDifference();
        });
        $('#complement').on('click', function (e) { //complement button
            e.preventDefault();
            app.onComplement();
        });
        $('#lparen').on('click', function (e) { //left paren button
            e.preventDefault();
            app.onLParen();
        });
        $('#rparen').on('click', function (e) { //right paren button
            e.preventDefault();
            app.onRParen();
        });
        $('#randomize').on('click', function (e) { //randomize button
            e.preventDefault();
            app.onRand();
        });
    },

    //loads schematic and generates workspace, sets up history to enable undo and redo
    setupSchematics: function () {
        var el = $('.simcir');
        if (el.data('schematicId')) {
            $.getJSON('processor.php?action=get_schematic&id=' + el.data('schematicId'), function (response) {
                app.setupEditor(response)
            }); //loads schematic
        } else {
            if (el.text() == '') {
                app.setupEditor();
            } else {
                var options = JSON.parse(el.text());
                app.setupEditor(options);
            }
        }
        app.setupSchematicMenu();

        // Event handler used for history management
        $(workspace).on('mouseup', function () {
            var current = simcir.controller(workspace).text();

            if (app.last != current) {
                // Schematic Changed
                setTimeout(function () {
                    app.history.push({ //saves existing simcir devices and connections
                        devices: simcir.controller(workspace).data().devices,
                        connections: simcir.controller(workspace).data().connections
                    });
                }, 500);
                app.history_index++;
                app.last = current;
            }
        })
    },

    //generates a blank workspace, acts as new
    resetSchematic: function () {
        app.setupEditor();
    },

    //loads schematic from file
    schematicImport: function () {
        function readSingleFile(evt) {
            //Retrieve the first (and only!) File from the FileList object
            var f = evt.target.files[0];

            if (f) {
                var r = new FileReader(); //uses Eli Grey's FileSaver library
                r.onload = function (e) {

                    var options = JSON.parse(e.target.result);
                    $('.modal').modal('hide'); //this is the json display feature from simcir, not the import wizard modal
                    app.setupEditor(options);
                };
                r.readAsText(f);
            } else {
                alert("Failed to load file");
            }
        }

        document.getElementById('data').addEventListener('change', readSingleFile, false);
    },

    //file dropdown button event handlers
    setupSchematicMenu: function () {
        // File Menu
        $('#new').on('click', function (e) { //new button in file dropdown
            e.preventDefault();
            app.resetSchematic();
        });
        $("#save").on('click', function (e) { //save button in file dropdown
            e.preventDefault();
            var el = $(this);
            var url = el.attr('href');
            var json = simcir.controller(workspace).text();
            $.post(url, JSON.parse(json), function (response) { //creates page post response containing simcir json for workspace
                if (response.code == 200) {
                    if (response.id) {
                        window.location = 'index.php?page=editor&id=' + response.id; //calls editor page with response
                    }
                    alert(response.msg);
                }
            })
        });
        $('#import').on('click', function (e) { //import button in file dropdown
            e.preventDefault();
            app.schematicImport();
        });
        $('#export').on('click', function (e) { //export button in file dropdown
            e.preventDefault();

            // Takes JSON from canvas
            var text = simcir.controller(workspace).text();

            // Builds file
            var blob = new Blob([text], {type: "application/json;charset=utf-8"});

            // Opens dialog to save file
            saveAs(blob, 'schematic.json');
        });

        // Edit Menu
        $('#undo').on('click', function (e) { //undo button in edit dropdown
            e.preventDefault();
            var item = app.history[app.history_index - 1];
            if (item) {
                app.history_index--;
                app.setupEditor(item);
            }
        });
        $('#redo').on('click', function (e) { //redo button in edit dropdown
            e.preventDefault();
            var item = app.history[app.history_index + 1];
            if (item) {
                app.setupEditor(item);
                app.history_index++;
            }
        });

        // Help Button
        $('#help').on('click', function (e) {
            e.preventDefault();
        });
    },

    //updates string visible to users for their notation input
    displayString: function (str2D) {
        $('#setOutputModal').val('{' + str2D + '}'); //input modal
        $('#setOutputEditor').val('{' + str2D + '}'); //main page
    },

    //functions called from event handlers
    onBack: function () {
        app.stringToParse = app.stringToParse.substring(0, app.stringToParse.length - 1); //removes last character from strings
        app.stringToDisplay = app.stringToDisplay.substring(0, app.stringToDisplay.length - 1);
        app.displayString(app.stringToDisplay); //updates visible
    },
    onClear: function () {
        app.stringToParse = ""; //sets strings as empty
        app.stringToDisplay = "";
        app.displayString(app.stringToDisplay); //updates visible
    },
    onEnter: function () {
        console.log("string length: " + app.stringToParse.length);
        if (app.stringToParse.length >= 1) {
            if (app.parseInput(app.stringToParse) == true) { //checks validity of input string
                app.genTree(); //makes tree
            }
            else {
                alert("The entered notation is not of the correct syntax.");
            }
        }
        else {
            alert("There is nothing to parse.");
        }
    },
    onA: function () {
        app.stringToParse = app.stringToParse + 'A'; //adds set a to string
        app.stringToDisplay = app.stringToDisplay + 'A';
        app.displayString(app.stringToDisplay);
    },
    onB: function () {
        app.stringToParse = app.stringToParse + 'B'; //adds set b to string
        app.stringToDisplay = app.stringToDisplay + 'B';
        app.displayString(app.stringToDisplay);
    },
    onC: function () {
        app.stringToParse = app.stringToParse + 'C'; //adds set c to string
        app.stringToDisplay = app.stringToDisplay + 'C';
        app.displayString(app.stringToDisplay);
    },
    onD: function () {
        app.stringToParse = app.stringToParse + 'D'; //adds set d to string
        app.stringToDisplay = app.stringToDisplay + 'D';
        app.displayString(app.stringToDisplay);
    },
    onE: function () {
        app.stringToParse = app.stringToParse + 'E'; //adds set e to string
        app.stringToDisplay = app.stringToDisplay + 'E';
        app.displayString(app.stringToDisplay);
    },
    onUnion: function () {
        app.stringToParse = app.stringToParse + 'u'; //adds union to string
        app.stringToDisplay = app.stringToDisplay + '⋃';
        app.displayString(app.stringToDisplay);
    },
    onIntersection: function () {
        app.stringToParse = app.stringToParse + 'n'; //adds intersection to string
        app.stringToDisplay = app.stringToDisplay + '∩';
        app.displayString(app.stringToDisplay);
    },
    onDifference: function () {
        app.stringToParse = app.stringToParse + 'f'; //adds difference to string
        app.stringToDisplay = app.stringToDisplay + '\\';
        app.displayString(app.stringToDisplay);
    },
    onComplement: function () {
        app.stringToParse = app.stringToParse + 'm'; //adds complement to string
        app.stringToDisplay = app.stringToDisplay + '￢';
        app.displayString(app.stringToDisplay);
    },
    onLParen: function () {
        app.stringToParse = app.stringToParse + '('; //adds left paren to string
        app.stringToDisplay = app.stringToDisplay + '(';
        app.displayString(app.stringToDisplay);
    },
    onRParen: function () {
        app.stringToParse = app.stringToParse + ')'; //adds right paren to string
        app.stringToDisplay = app.stringToDisplay + ')';
        app.displayString(app.stringToDisplay);
    },

    //randomization function, keeps format of original string, only changes sets and binary operators
    onRand: function () {
        if(app.stringToParse.length>0&&app.parseInput(app.stringToParse)) //checks validity of input string
        {
            var randString=""; //new string
            for(var i=0; i<app.stringToParse.length; i++)
            {
                if(app.stringToParse[i]=='m'||app.stringToParse[i]=='('||app.stringToParse[i]==')') //does not randomize unary ops or parens
                {
                    randString=randString+app.stringToParse[i];
                }
                else if(app.stringToParse[i]=='A'||app.stringToParse[i]=='B'||app.stringToParse[i]=='C'||app.stringToParse[i]=='D'||app.stringToParse[i]=='E')
                {
                    randString=randString+app.terminals[Math.floor(Math.random() * app.terminals.length)]; //adds random set
                }
                else if(app.stringToParse[i]=='u'||app.stringToParse[i]=='n'||app.stringToParse[i]=='f')
                {
                    randString=randString+app.basicOperators[Math.floor(Math.random() * app.basicOperators.length)]; //adds random binary operator
                }
            }
            var newDisplayString=app.parseToDisplay(randString); //creates display string
            console.log("new rand str to parse "+randString);
            console.log("new rand str to display "+newDisplayString);
            app.stringToParse=randString; //strings updated
            app.stringToDisplay=newDisplayString;
            app.displayString(app.stringToDisplay);
        }
    },

    //parses input for syntax
    parseInput: function (str2P) {
        console.log("\n" + str2P);
        var currentcharindex = 0;
        var nextcharindex = 0;
        if (app.isLP(str2P[currentcharindex]) || app.isTerm(str2P[currentcharindex]) || app.isComp(str2P[currentcharindex])) //char 1 check
        {//1st char must be valid, so no binary ops or right parens
            console.log("1st char valid");
            if (str2P.length == 1) //length check 2
            {
                console.log("single char type check ");
                if (app.isTerm(str2P[currentcharindex])) //checks if single char is terminal
                    return true;
            }
            nextcharindex = 1;
            var checkBalance=0;
            for (var j = 0; j < str2P.length; j++) { //checks if parens are balanced
                if (app.isLP(str2P[j])) {
                    checkBalance++;
                }
                else if (app.isRP(str2P[j])) {
                    checkBalance--;
                }
            }
            if(checkBalance!=0)
            {
                console.log(checkBalance);
                return false;
            }
            console.log("entering for loop 1");
            for (currentcharindex; currentcharindex < str2P.length - 1; currentcharindex++) {
                console.log("Current char: " + str2P[currentcharindex] + " at " + currentcharindex);
                if (app.isLP(str2P[currentcharindex])) { //if a left paren is found
                    //recursive witchcraft afoot
                    //for loop to find closest right paren
                    var balance = 0;
                    console.log("entering paren check loop");
                    for ( j = currentcharindex + 1; j < str2P.length - 1; j++) {
                        console.log("current balance: " + balance);
                        if (app.isLP(str2P[j])) {
                            balance++;
                        }                        else if (app.isRP(str2P[j])) {
                            balance--;
                        }

                        if (balance == 0)
                            break;
                    }
                    console.log("passing " + currentcharindex + 1 + " to " + j - (currentcharindex + 1) + " to recursive call");
                    if(!app.parseInput(str2P.slice(currentcharindex + 1, j - (currentcharindex + 1))))  //cuts out parens and checks their contents
                    {
                        return false;
                    }
                    currentcharindex = j;
                    nextcharindex = currentcharindex + 1;
                }
                else if (app.isTerm(str2P[currentcharindex])) { //sets must be followed by a right paren or a binary operator
                    if (currentcharindex < str2P.length - 1) {
                        if (app.isRP(str2P[nextcharindex]) == true) {
                            //depends on handling parens
                        }
                        else if (app.isOp(str2P[nextcharindex]) == false) {
                            return false;
                        }
                    }
                }
                else if (app.isOp(str2P[currentcharindex])) { //binary operators must be followed by a set, unary operator, or left paren
                    if (currentcharindex < str2P.length - 1) {
                        if (app.isOp(str2P[nextcharindex]) || app.isRP(str2P[nextcharindex]) == true) {
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                else if (app.isComp(str2P[currentcharindex])) { //unary operator must be followed by a left paren or set
                    if (currentcharindex < str2P.length - 1) {
                        if (app.isOp(str2P[nextcharindex]) || app.isRP(str2P[nextcharindex]) == true) {
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
                else if (app.isRP(str2P[currentcharindex])) { //right paren must be followed by a binary operator
                    if (app.isLP(str2P[nextcharindex]) || app.isTerm(str2P[nextcharindex]) == true || app.isComp(str2P[nextcharindex])) {
                        return false;
                    }
                }
                nextcharindex++;
            }
            console.log("Final char: " + str2P[currentcharindex] + " at " + currentcharindex); //final char tested separately
            if (app.isLP(str2P[currentcharindex]) || app.isOp(str2P[currentcharindex]) || app.isComp(str2P[currentcharindex])) {
                return false;
            }
            //alert("Successfully parsed for syntax.");
            return true;
        }
        else {
            return false;
        }
    },

    //processes the string and outputs the final result
    genTree: function () {
        app.treeElements=[];
        app.treeTiers=[];
        app.defineSets(); //fills arrays based on user input in the sets checkboxes
        if (app.setA.length + app.setB.length + app.setC.length + app.setD.length + app.setE.length == 0) {
            alert("All sets are empty.");
        }
        else {
            var toSave = app.statCalc(app.stringToParse); //totals inputs for sending to the database
            $.post('processor.php?action=save_stats', { //kinda broke, maybe? occasional errors, dont know why
                data: {
                    users: {
                        id: window.App.user_id,
                        count_a: toSave[0],
                        count_b: toSave[1],
                        count_c: toSave[2],
                        count_d: toSave[3],
                        count_e: toSave[4],
                        count_union: toSave[5],
                        count_inter: toSave[6],
                        count_diff: toSave[7],
                        count_comp: toSave[8],
                        max_depth: toSave[9] //should be more intelligently done, doesn't actually save max now, just the last
                    }
                }
            }, function (callback) {
                console.log(callback);
            });
            app.postfixEval(app.infixToPostfix(app.stringToParse)); //converts to postfix, creates tree from it
            console.log("elements: "+app.treeElements.length);
            for(var x = 0; x < app.treeElements.length; x++) //outputs tree contents
            {
                for(var nodeD=0; nodeD<app.treeElements[x].data.length; nodeD++)
                {
                    console.log("node data: "+app.treeElements[x].data[nodeD]);
                }
                console.log("parent index: "+app.treeElements[x].parent);
                for(var nodeC=0; nodeC<app.treeElements[x].children.length; nodeC++)
                {
                    console.log("node children: "+app.treeElements[x].children[nodeC]);
                }
            }
            var final = app.makeSchematicJSON(); //creates the JSON for Simcir
            var options = JSON.parse(final);
            app.setupEditor(options); //creates new workspace with the schematic
        }
    },
    isTerm: function (testChar) { //checks if a character is in the set of sets
        console.log("testing for term with: " + testChar);
        for (var i = 0; i < app.terminals.length; i++) {
            if (testChar == app.terminals[i]) {
                return true;
            }
        }
        return false;
    },
    isOp: function (testChar) { //checks if a char is a binary operator
        console.log("testing for op with: " + testChar);
        for (var i = 0; i < app.basicOperators.length; i++) {
            if (testChar == app.basicOperators[i]) {
                return true;
            }
        }
        return false;
    },
    isComp: function (testChar) { //checks if a char is the complement operator
        console.log("testing for comp with: " + testChar);
        if (testChar == app.compOperator) {
            return true;
        }
        else {
            return false;
        }
    },
    isLP: function (testChar) { //checks if a char is a left paren
        console.log("testing for LP with: " + testChar);
        if (testChar == "(") {
            return true;
        }
        else {
            return false;
        }
    },
    isRP: function (testChar) { //checks if a char is a right paren
        console.log("testing for RP with: " + testChar);
        if (testChar == ")") {
            return true;
        }
        else {
            return false;
        }
    },

    //gets the checkboxes to define sets
    defineSets: function () {
        app.setA = "";
        app.setB = "";
        app.setC = "";
        app.setD = "";
        app.setE = "";

        //A
        if ($('#AaToggle').is(":checked")) {
            app.setA = app.setA + 'a';
        }
        if ($('#AbToggle').is(":checked")) {
            app.setA = app.setA + 'b';
        }
        if ($('#AcToggle').is(":checked")) {
            app.setA = app.setA + 'c';
        }
        if ($('#AdToggle').is(":checked")) {
            app.setA = app.setA + 'd';
        }
        if ($('#AeToggle').is(":checked")) {
            app.setA = app.setA + 'e';
        }
        if ($('#AfToggle').is(":checked")) {
            app.setA = app.setA + 'f';
        }
        if ($('#AgToggle').is(":checked")) {
            app.setA = app.setA + 'g';
        }
        if ($('#AhToggle').is(":checked")) {
            app.setA = app.setA + 'h';
        }
        if ($('#AiToggle').is(":checked")) {
            app.setA = app.setA + 'i';
        }
        if ($('#AjToggle').is(":checked")) {
            app.setA = app.setA + 'j';
        }

        //B
        if ($('#BaToggle').is(":checked")) {
            app.setB = app.setB + 'a';
        }
        if ($('#BbToggle').is(":checked")) {
            app.setB = app.setB + 'b';
        }
        if ($('#BcToggle').is(":checked")) {
            app.setB = app.setB + 'c';
        }
        if ($('#BdToggle').is(":checked")) {
            app.setB = app.setB + 'd';
        }
        if ($('#BeToggle').is(":checked")) {
            app.setB = app.setB + 'e';
        }
        if ($('#BfToggle').is(":checked")) {
            app.setB = app.setB + 'f';
        }
        if ($('#BgToggle').is(":checked")) {
            app.setB = app.setB + 'g';
        }
        if ($('#BhToggle').is(":checked")) {
            app.setB = app.setB + 'h';
        }
        if ($('#BiToggle').is(":checked")) {
            app.setB = app.setB + 'i';
        }
        if ($('#BjToggle').is(":checked")) {
            app.setB = app.setB + 'j';
        }

        //C
        if ($('#CaToggle').is(":checked")) {
            app.setC = app.setC + 'a';
        }
        if ($('#CbToggle').is(":checked")) {
            app.setC = app.setC + 'b';
        }
        if ($('#CcToggle').is(":checked")) {
            app.setC = app.setC + 'c';
        }
        if ($('#CdToggle').is(":checked")) {
            app.setC = app.setC + 'd';
        }
        if ($('#CeToggle').is(":checked")) {
            app.setC = app.setC + 'e';
        }
        if ($('#CfToggle').is(":checked")) {
            app.setC = app.setC + 'f';
        }
        if ($('#CgToggle').is(":checked")) {
            app.setC = app.setC + 'g';
        }
        if ($('#ChToggle').is(":checked")) {
            app.setC = app.setC + 'h';
        }
        if ($('#CiToggle').is(":checked")) {
            app.setC = app.setC + 'i';
        }
        if ($('#CjToggle').is(":checked")) {
            app.setC = app.setC + 'j';
        }

        //D
        if ($('#DaToggle').is(":checked")) {
            app.setD = app.setD + 'a';
        }
        if ($('#DbToggle').is(":checked")) {
            app.setD = app.setD + 'b';
        }
        if ($('#DcToggle').is(":checked")) {
            app.setD = app.setD + 'c';
        }
        if ($('#DdToggle').is(":checked")) {
            app.setD = app.setD + 'd';
        }
        if ($('#DeToggle').is(":checked")) {
            app.setD = app.setD + 'e';
        }
        if ($('#DfToggle').is(":checked")) {
            app.setD = app.setD + 'f';
        }
        if ($('#DgToggle').is(":checked")) {
            app.setD = app.setD + 'g';
        }
        if ($('#DhToggle').is(":checked")) {
            app.setD = app.setD + 'h';
        }
        if ($('#DiToggle').is(":checked")) {
            app.setD = app.setD + 'i';
        }
        if ($('#DjToggle').is(":checked")) {
            app.setD = app.setD + 'j';
        }

        //E
        if ($('#EaToggle').is(":checked")) {
            app.setE = app.setE + 'a';
        }
        if ($('#EbToggle').is(":checked")) {
            app.setE = app.setE + 'b';
        }
        if ($('#EcToggle').is(":checked")) {
            app.setE = app.setE + 'c';
        }
        if ($('#EdToggle').is(":checked")) {
            app.setE = app.setE + 'd';
        }
        if ($('#EeToggle').is(":checked")) {
            app.setE = app.setE + 'e';
        }
        if ($('#EfToggle').is(":checked")) {
            app.setE = app.setE + 'f';
        }
        if ($('#EgToggle').is(":checked")) {
            app.setE = app.setE + 'g';
        }
        if ($('#EhToggle').is(":checked")) {
            app.setE = app.setE + 'h';
        }
        if ($('#EiToggle').is(":checked")) {
            app.setE = app.setE + 'i';
        }
        if ($('#EjToggle').is(":checked")) {
            app.setE = app.setE + 'j';
        }

        //logging output
        for (var i = 0; i < app.setA.length; i++) {
            console.log("Set A: " + app.setA[i]);
        }
        for (i = 0; i < app.setB.length; i++) {
            console.log("Set B: " + app.setB[i]);
        }
        for (i = 0; i < app.setC.length; i++) {
            console.log("Set C: " + app.setC[i]);
        }
        for (i = 0; i < app.setD.length; i++) {
            console.log("Set D: " + app.setD[i]);
        }
        for (i = 0; i < app.setE.length; i++) {
            console.log("Set E: " + app.setE[i]);
        }
        console.log("AB Union: " + app.calcUnion(app.setA, app.setB));
        console.log("AB Intersection: " + app.calcIntersection(app.setA, app.setB));
        console.log("AB Difference: " + app.calcDifference(app.setA, app.setB));
        console.log("A Complement: " + app.calcComplement(app.setA));
    },

    //takes two sets and outputs their union
    calcUnion: function (set1, set2) {
        var resultSet = "";
        for (var i = 0; i < set1.length; i++) { //adds all from set 1
            resultSet = resultSet + set1[i];
        }
        for (i = 0; i < set2.length; i++) {
            var inChecks = false;
            for (var j = 0; j < resultSet.length; j++) { //if element from set isn't already present, add it
                if (resultSet[j] == set2[i]) {
                    inChecks = true;
                }
            }
            if (inChecks == false) {
                resultSet = resultSet + set2[i];
            }
        }
        resultSet = app.sortAlpha(resultSet); //organizes the set contents alphabetically
        return resultSet;
    },

    //takes two sets and outputs the intersection
    calcIntersection: function (set1, set2) {
        var resultSet = "";
        for (var i = 0; i < set1.length; i++) {
            for (var j = 0; j < set2.length; j++) { //adds to new set if element exists in both input sets
                if (set1[i] == set2[j]) {
                    resultSet = resultSet + set1[i];
                }
            }
        }
        resultSet = app.sortAlpha(resultSet); //organizes the set contents alphabetically
        return resultSet;
    },

    //takes two sets and outputs the difference
    calcDifference: function (set1, set2) {
        var resultSet = "";
        for (var i = 0; i < set1.length; i++) {
            var found = false;
            for (var j = 0; j < set2.length; j++) { //if element from set1 is not in set 2, it gets added to output
                if (set1[i] == set2[j]) {
                    found = true;
                }
            }
            if (found == false) {
                resultSet = resultSet + set1[i];
            }
        }
        resultSet = app.sortAlpha(resultSet); //organizes the set contents alphabetically
        return resultSet;
    },

    //outputs the complement of an input set
    calcComplement: function (set1) {
        var resultSet = "";
        for (var i = 0; i < app.universalSet.length; i++) {
            var found = false;
            for (var j = 0; j < set1.length; j++) {
                if (app.universalSet[i] == set1[j]) { //if element from universal set is not in the input set, add it to output
                    found = true;
                }
            }
            if (found == false) {
                resultSet = resultSet + app.universalSet[i];
            }
        }
        resultSet = app.sortAlpha(resultSet); //organizes the set contents alphabetically
        return resultSet;
    },
    sortAlpha: function (str) {
        return str.split('').sort().join('');
    },

    //postfixEval adapted from Dmitry Soshnikov
    //https://gist.github.com/DmitrySoshnikov/1238301
    postfixEval: function (string) {
        console.log("in eval");
        var operators = { //defines action for binary operators
            "u": function (a, b) {
                return app.calcUnion(a, b)
            },
            "f": function (a, b) {
                return app.calcDifference(a, b)
            },
            "n": function (a, b) {
                return app.calcIntersection(a, b)
            }
        };
        var compOp = { //defines action for unary operator
            "m": function (a) {
                return app.calcComplement(a)
            }
        };
        var sets = { //defines action for set terminals
            "A": function () {
                return app.setA
            },
            "B": function () {
                return app.setB
            },
            "C": function () {
                return app.setC
            },
            "D": function () {
                return app.setD
            },
            "E": function () {
                return app.setE
            }
        };
        var stack = [];
        var ch; // current char
        var nodes =[]; //array of all nodes created
        var nodeNumber = 0; //acts as an index and id
        var nodeStack = [];
        for (var i = 0; i < string.length; i++) { //loops through input string
            ch = string[i];
            console.log("char ", i, " :", ch);
            // if it's a value, push it onto the stack
            if (ch in sets) {
                var value = sets[ch]();
                stack.push(value); //pushes set value
                nodes.push(new Node([nodeNumber, value, ch], null, [])); //creates new node with set value, id, and operator
                nodeStack.push(nodes[nodeNumber]); //pushes new node
                nodeNumber++;
            }
            // else if it's a unary operator
            else if (ch in compOp) {
                var a = stack.pop(); //pops value from stack
                value = compOp[ch](a); //gets result of complement
                stack.push(value); //pushes new value
                nodes.push(new Node([nodeNumber, value, ch], null, [])); //new node created like above

                var tempNode=nodeStack.pop(); //sets children and parents of node
                nodes[nodeNumber].children=[tempNode.data[0]];
                nodes[tempNode.data[0]].parent=nodes[nodeNumber].data[0];
                nodeStack.push(nodes[nodeNumber]);

                nodeNumber++;
                console.log(value);
            }
            // else if it's a binary operator
            else if (ch in operators) {
                var b = stack.pop(); //pops values from stack
                a = stack.pop();
                console.log(b);
                console.log(a);
                value = operators[ch](a, b);
                stack.push(value); //pushes new value resulting from operator

                nodes.push(new Node([nodeNumber, value, ch], null, [])); //new node created like before

                var tempNode1=nodeStack.pop(); //parents and children assigned
                var tempNode2=nodeStack.pop();
                nodes[nodeNumber].children=[tempNode2.data[0],tempNode1.data[0]];
                nodes[tempNode1.data[0]].parent=nodes[nodeNumber].data[0];
                nodes[tempNode2.data[0]].parent=nodes[nodeNumber].data[0];
                nodeStack.push(nodes[nodeNumber]);

                nodeNumber++;
                console.log(value);
            }
        }
        if (stack.length > 1) { //error catching
            console.log("postfixEval error - stack greater than 1");
        }
        console.log(stack[0]);
        app.treeRoot=new Tree(); //not really used
        app.treeRoot._root=nodeStack[0];
        app.treeElements=nodes;
        return stack[0]; //returns the final value, not used
    },
    //infixToPostfix adapted from Dmitry Soshnikov
    //https://gist.github.com/DmitrySoshnikov/1239804
    infixToPostfix: function (reStr, dontPrint) {
        Array.prototype.peek = function () { //made writing easier, just returns the last in the array
            return this[this.length - 1];
        };
        var precedenceMap = { //sets up order of operations
            '(': 1,
            'm': 2, // complement
            'n': 3, // intersection
            'f': 3, // difference
            'u': 4 // union
            // else 5
        };

        function precedenceOf(c) {
            return precedenceMap[c] || 5; //returns value from above
        }

        var output = [];
        var stack = [];
        for (var k = 0, length = reStr.length; k < length; k++) {
            // current char
            var c = reStr[k];
            if (c == '(')
                stack.push(c);
            else if (c == ')') {
                while (stack.peek() != '(') {
                    output.push(stack.pop())
                }
                stack.pop(); // pop '('
            }
            // else work with the stack
            else {
                while (stack.length) {
                    var peekedChar = stack.peek();
                    var peekedCharPrecedence = precedenceOf(peekedChar);
                    var currentCharPrecedence = precedenceOf(c);
                    if (peekedCharPrecedence >= currentCharPrecedence) {
                        output.push(stack.pop());
                    } else {
                        break;
                    }
                }
                stack.push(c);
            }
        } // end for loop
        while (stack.length) {
            output.push(stack.pop());
        }
        var result = output.join("");
        !dontPrint && console.log(reStr, "=>", result);
        return result;
    },

    //basic stats calculation, counts the number of each char in a string
    statCalc: function (string) {
        var a = 0;
        var b = 0;
        var c = 0;
        var d = 0;
        var e = 0;
        var union = 0;
        var diff = 0;
        var inter = 0;
        var comp = 0;
        var depth = 1;
        var max = 1;
        for (var i = 0; i < string.length; i++) {
            if (string[i] == "A") {
                a++;
            }
            if (string[i] == "B") {
                b++;
            }
            if (string[i] == "C") {
                c++;
            }
            if (string[i] == "D") {
                d++;
            }
            if (string[i] == "E") {
                e++;
            }
            if (string[i] == "u") {
                union++;
            }
            if (string[i] == "f") {
                diff++;
            }
            if (string[i] == "n") {
                inter++;
            }
            if (string[i] == "m") {
                comp++;
            }
        }
        for (i = 0; i < string.length; i++) {
            if (string[i] == "(") {
                depth++;
            }
            if (string[i] == ")") {
                depth--;
            }
            if (depth > max) {
                max = depth;
            }
        }
        var data = [a, b, c, d, e, union, inter, diff, comp, max];
        return data;
    },

    //creates JSON needed for simcir to define the workspace
    makeSchematicJSON: function () {
        var toOutput =
        {
            width: window.innerWidth, //workspace size
            height: window.innerHeight - 100,
            showToolbox: true, //sidebar
            toolbox: [ //elements in sidebar
                {"type": "Set A"},
                {"type": "Set B"},
                {"type": "Set C"},
                {"type": "Set D"},
                {"type": "Set E"},
                {"type": "Complement"},
                {"type": "Union"},
                {"type": "Intersection"},
                {"type": "Difference"}
            ],
            devices: [], //elements in workspace
            connectors: [] //connections between elements
        };
        var rootIndex = -1;
        var rootCount = 0;
        for (var i = 0; i < app.treeElements.length; i++) {
            if (app.treeElements[i].parent == null) { //finds tree root, in case last node is not root for some reason
                rootCount++;
                rootIndex = i;
            }
        }
        if (rootCount > 1) { //error catching
            console.log("too many roots");
        }
        else if (rootCount < 1) {
            console.log("root missing");
        }
        if (rootIndex < 0) {
            return false;
        }
        var newTreeTier = [];
        newTreeTier.push(app.treeElements[rootIndex]); //level 1 of tree created
        app.treeTiers.push(newTreeTier);
        app.getChildren(app.treeElements[rootIndex]); //creates other heights of tree from root

        for(i=0; i<app.treeTiers.length; i++)
        {
            for(var j=0; j<app.treeTiers[i].length; j++) //goes by tree height
            {
                var newDevice={ //creates element for devices array
                    "type":app.getType(app.treeTiers[i][j]),
                    "id":"dev"+app.treeTiers[i][j].data[0],
                    "x":app.corner+(((app.treeTiers.length-i)-1)*app.xSpacing),
                    "y":app.corner+(app.treeTiers[i].length-(app.treeTiers[i].length-j))*app.ySpacing+(32*Math.pow(2,((app.treeTiers.length-i)-1))),
                    "label":app.treeTiers[i][j].data[1]
                };
                toOutput.devices.push(newDevice);
                if(app.treeTiers[i][j].children.length>0) {
                    for (var k = 0; k < app.treeTiers[i][j].children.length; k++) { //creates element for connectors array
                        var newConnection={
                            "from":"dev"+app.treeTiers[i][j].data[0]+".in"+k,
                            "to":"dev"+app.treeTiers[i][j].children[k]+".out0"
                        };
                        toOutput.connectors.push(newConnection);
                    }
                }
            }
        }
        var finalJSON = JSON.stringify(toOutput);
        console.log(finalJSON);
        return finalJSON;
    },

    //fills out tree from root
    getChildren: function (rootNode) {
        //console.log("rootnode in getchildren: "+rootNode);
        var newTreeTier = [];
        var childQueue = []; //nodes being processed
        var nextGenQueue = []; //nodes to be processed
        if(rootNode.children.length>0){
            for(var i=0; i<rootNode.children.length; i++) //adds children of root to be processed
            {
                childQueue.push(app.treeElements[rootNode.children[i]]);
            }
            //console.log("childQueue"+childQueue);
            app.treeTiers.push(childQueue); //tier 2 of tree done
            while(childQueue.length!=0) //children now being checked
            {
                for(var j=0; j<childQueue.length; j++)
                {
                    //console.log("\nchildQueue"+childQueue+"\n");
                    if(childQueue[j].children.length>0) { //adds children to be processed
                        for (var k = 0; k < childQueue[j].children.length; k++) {
                            nextGenQueue.push(app.treeElements[childQueue[j].children[k]]);
                        }
                    }
                }
                if(nextGenQueue.length>0)
                {
                    app.treeTiers.push(nextGenQueue); //pushes children to next tier of tree
                }
                childQueue = nextGenQueue;
                nextGenQueue = []; //begins processing children
            }
        }
        console.log("\ntiers: " + app.treeTiers.length);
        for(var x = 0; x < app.treeTiers.length; x++) //outputs constructed tree to console for checking manually
        {
            console.log("elements in tier "+ x + " : "+ app.treeTiers[x].length);
            for(var y = 0; y < app.treeTiers[x].length; y++)
            {
                for(var nodeD=0; nodeD<app.treeTiers[x][y].data.length; nodeD++)
                {
                    console.log("node data: "+app.treeTiers[x][y].data[nodeD]);
                }
                console.log("parent index: "+app.treeTiers[x][y].parent);
                if(app.treeTiers[x][y].children.length<1)
                {
                    console.log("Node has no children");
                }
                else {
                    for (var nodeC = 0; nodeC < app.treeTiers[x][y].children.length; nodeC++) {
                        console.log("node children: " + app.treeTiers[x][y].children[nodeC]);
                    }
                }
            }
        }
    },

    //creates the unicode containing string from the standard ascii string
    parseToDisplay: function (string) {
        var newDisplayString="";
        for(var i=0; i<string.length; i++)
        {
            if(string[i]!='u'&&string[i]!='f'&&string[i]!='n'&&string[i]!='m')
            {
                newDisplayString=newDisplayString+string[i];
            }
            else if(string[i]=='u')
            {
                newDisplayString=newDisplayString+'⋃';
            }
            else if(string[i]=='f')
            {
                newDisplayString=newDisplayString+'\\';
            }
            else if(string[i]=='n')
            {
                newDisplayString=newDisplayString+'∩';
            }
            else if(string[i]=='m')
            {
                newDisplayString=newDisplayString+'￢';
            }
        }
        return newDisplayString;
    },

    //returns string describing char
    getType: function (n) {
        if(n.data[2]=="A")
        {
            return "Set A";
        }
        else if(n.data[2]=="B")
        {
            return "Set B";
        }
        else if(n.data[2]=="C")
        {
            return "Set C";
        }
        else if(n.data[2]=="D")
        {
            return "Set D";
        }
        else if(n.data[2]=="E")
        {
            return "Set E";
        }
        else if(n.data[2]=="u")
        {
            return "Union";
        }
        else if(n.data[2]=="n")
        {
            return "Intersection";
        }
        else if(n.data[2]=="f")
        {
            return "Difference";
        }
        else if(n.data[2]=="m")
        {
            return "Complement";
        }
        else
        {
            console.log("error finding node type");
            return "error";
        }
    }
};
app.init();