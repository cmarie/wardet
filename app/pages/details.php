<div class="container">
    <h1>Project Details</h1>
    
    <h2>Project Description 2.0</h2>
    
    <p>The operations performed on sets are similar to SQL operations that manipulate databases. Many students in discrete math courses have difficulty understanding expressions involving sets. They just might find a tool that gives feedback on set operations helpful in later understanding SQL expressions.</p>
    
    <p>Design a visual editor that allows users to build circuits of their choice and a means to follow the logic flowing through the circuit.</p>
    
    <h2>Project Requirements</h2>
    <ol>
        <li>Ability to visually and easily build and edit an expression using set notation.</li>
        <li>Use of union, intersection, complement, difference, including nested parentheses.</li>
        <li>Ability to see final result, order, animation, stop, backup, redo</li>
        <li>Randomly or smartly pick selections.</li>
        <li>Include basic controls like new, open, save, delete</li>
        <li>Some form of statistics calculation.</li>
    </ol>
    
    
    <h1>---</h1>
    
    
    <h2>Project Description 1.0</h2>
    <p>Design a visual editor that allows users to build circuits of their choice and a means to follow the logic flowing through the circuit.</p>
    
    <h2>Project Requirements</h2>
    <ol>
        <li>Obtain design ideas from editors like Lego's Mindstorm.</li>
        <li>Provide visual NOT, AND, NAND, OR, NOR, XOR and IMP gates.</li>
        <li>Allow users to provide inputs and follow outputs.</li>
        <li>Show the logic flow through each gate of the circuit. The user can step through the circuit one gate at a time, execute up to a given gate, and execute the entire circuit by setting the rate of flow.</li>
        <li>Provide a meta-circuit builder that forms new circuits from existing configurations. The new circuits can then be used in circuit construction.</li>
        <li>Provide functions to move, copy, paste, insert, new, delete, save, open, print.</li>
        <li>Of course, provide example circuits and a help option.</li>
    </ol>
</div>