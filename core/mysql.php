<?php
    
    class MySQL
    {
        private $_conn;
        
        function __construct($_server, $_username, $_password, $_db)
        {
            $this->_conn = new mysqli($_server, $_username, $_password, $_db);
            if(!$this->_conn) {
                die("Connection failed: " . mysqli_connect_error());
            }
            //debug($this->select('SELECT * FROM users'));
        }
        
        public function insert($_data = [])
        {
            $callback = false;
            $sql = $this->__parseData($_data, true);
            if($this->_conn->query($sql) === true) {
                $callback = $this->_conn->insert_id;
            }
            
            return $callback;
        }
        
        public function update($_data = [])
        {
            $callback = false;
            $sql = $this->__parseData($_data);
            if($this->_conn->query($sql) === true) {
                $callback = true;
            }
            
            return $callback;
        }
        
        public function find($_type = '', $_table = '', $_conditions = [])
        {
            $conditions = '';
            foreach($_conditions as $field => $value) {
                $conditions .= sprintf('`%s` = "%s" AND ', $field, $value);
            }
            if(!empty($conditions)) {
                $conditions = substr($conditions, 0, strlen($conditions) - 5);
                $sql = sprintf("SELECT * FROM `%s` WHERE %s", $_table, $conditions);
            } else {
                $sql = sprintf("SELECT * FROM `%s`", $_table);
            }
            if($_type == 'all') {
                return $this->select($sql);
            } elseif($_type == 'first') {
                return $this->_conn->query($sql)->fetch_assoc();
            }
            
            return false;
        }
        
        public function read($_table = '', $_id = null)
        {
            return $this->_conn->query(sprintf("SELECT * FROM `%s` WHERE `id` = %s", $_table, $_id))->fetch_assoc();
        }
        
        public function select($sql = '')
        {
            $callback = [];
            foreach($this->_conn->query($sql, MYSQLI_ASSOC) as $item) {
                $callback[] = $item;
            }
            
            return $callback;
        }
        
        public function delete($_table = '', $_id)
        {
            $callback = false;
            $sql = sprintf("DELETE FROM `%s` WHERE `id` = %s", $_table, $_id);
            if($this->_conn->query($sql) === true) {
                $callback = true;
            }
            
            return $callback;
        }
        
        private function __schema($_table = '')
        {
            $sql = sprintf("SHOW COLUMNS FROM %s", $_table);
            $fields = [];
            foreach($this->_conn->query($sql) as $column) {
                $field = $column['Field'];
                $value = trim($column['Type']);
                preg_match('!\(([^\)]+)\)!', $value, $match);
                $fields[$field]['type'] = explode('(', $value)[0];
                $fields[$field]['length'] = @$match[1];
            }
            
            return $fields;
        }
        
        private function __parseData($_data = [], $insert = false)
        {
            $table = array_keys($_data)[0];
            if(array_key_exists('user_id', $this->__schema($table)) && empty($_data[$table]['user_id'])) {
                $_data[$table]['user_id'] = $_SESSION['User']['id'];
            }
            if($insert) {
                $_data[$table]['created'] = date('Y-m-d H:i:s');
            }
            $_data[$table]['modified'] = date('Y-m-d H:i:s');
            if($insert) {
                $columns = implode("`, `", array_keys($_data[$table]));
                $values = [];
                foreach(array_values($_data[$table]) as $value) {
                    $values[] = addslashes($value);
                }
                $values = implode("', '", $values);
                
                return sprintf("INSERT INTO `%s` (`%s`) VALUES ('%s')", $table, $columns, $values);
            } else {
                $fields = '';
                foreach($_data[$table] as $field => $value) {
                    $fields .= sprintf("`%s` = '%s', ", $field, addslashes($value));
                }
                $fields = substr($fields, 0, strlen($fields) - 2);
                
                return sprintf("UPDATE `%s` SET %s WHERE `id` = %s", $table, $fields, $_data[$table]['id']);
            }
        }
    }