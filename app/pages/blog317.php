<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>A preliminary version of data validation is running for the input now, but it does not work correctly. It would seem as though I have an if statement that is not executing, but I do not yet know why. Overall, I'm not thrilled with how my syntax parser is designed, but I just need something resembling progress at this point. It recognized that single terminals were ok. This also allowed me to find other errors in my input buttons while attempting to test the parser. As I see it, for this project to even resemble completion, this is one of three major issues left to tackle. The next is splitting the input into a parse tree, followed by visualizing the parse tree.</p>

</div>