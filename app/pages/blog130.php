<div class="container">
    <h2>Ethan Ward's Project Blog</h2>

    <p>At the time of writing this, there is not much to see on my website, as it is still under construction. This will be fixed by the end of the week. My goal with this first writing is to explain my project, my goals for the project, my current plans, and some other notes.</p>


    <p>To begin then, what is my assigned task? I have been tasked with the creation of a program that allows a user to drag and drop elements of basic digital circuits (logic gates) onto a workspace to create circuits. The program will need to include functionality for editing these circuits, loading and saving the work of a user, and have the ability to step through a circuit to see how it derives its current state (on/off). Harder still is the requirement of allowing users to save circuits as components to use in other circuits. For example, someone could create a series of gates which serve some purpose, but the series will appear multiple times in the circuit. This could be condensed into a single component for ease of use. Including examples and help documentation will be the final touch.</p>

    <p>My first thought with this project is that I have no idea where to begin. There are a number of choices that are in front of me at the moment, and these will determine the course of the entire project (for better or worse). First, the project requirements given to me do not specify a platform to make this program on. I see my choices here as programming a Windows application in C#, or creating a web based application in HTML5. I have work experience with C#, but I also know that my ability with graphical output in C# will hinder my efforts. HTML5 is a great unknown. I have never attempted programming anything graphical in HTML, so this would be a major leap into the unknown. HTML5 would bring the added benefit of some platform independence though, so it is a consideration given that the description has this project oriented more as a learning tool. Learning tools are better when they are accessible.</p>

    <p>With that in mind, I have created a preliminary checklist of components I will need to include:</p>

    <ol>
        <li>Some kind of workspace, preferably a grid that components would snap to, and ideally would be able to extend "off the screen" like a spreadsheet or a map.</li>
        <li>Visual icons for the various logic gates, wires connecting them, and some kind of "power" nodes that would trigger circuits.</li>
        <li>An underlying data container to describe parts, their positions, their inputs, their outputs, and what they are connected to. This will likely need revision as I progress.</li>
        <li>Controls to step through the circuit starting from the power nodes either manually (component by component), or automatically progressing through the components over some time interval.</li>
        <li>Controls to move, copy, paste, insert, new, delete, save, open, print.</li>
        <li>Some mechanism for simplifying circuits and saving them as components. This is one of the more daunting tasks. I am not sure how to approach this. My working requirements for condensing components loosely includes:
            <ol>
                <li>Some maximum number of components</li>
                <li>A minimum number of components (most likely two, plus wires).</li>
                <li>A method of detecting whether saved components are immediately connected.</li>
                <li>A way of ordering and executing the component logic gates.</li>
                <li>Some form of icon representing the condensed components.</li>
            </ol>
        </li>
        <li>Some algorithm for finding the start of a circuit.</li>
        <li>Lots of documentation</li>
        <li>Example circuits for the user to learn from - Would require the load feature to work</li>
    </ol>

    <li>My preliminary research has looked at creating drawing spaces in HTML5, the standard iconography for logic gates, and some refresher material on C#. While not an overly specific list, it is a start.</li>

    <li>My goals for the week are to have the platform picked, a basic workspace created in whichever is selected, the website will be uploaded, and of course, more research will need to be done. </li>

    <li>This looks as though it will be a challenging project, and while I'm absolutely terrified about the time constraints and other obligations in other courses, I am looking forward to producing something of value.</li>
</div>