<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>This past week was very slow, entirely due to illness. I did not accomplish all I was after, but progress was made on the back end. I have been looking at parsing algorithms for the user input, and have a good solution underway for handling the user input. Once all of that is complete, the next step will be figuring out how to take the result of the parsing and make something visual out of it.</p>
    
    <p>The work so far with the SimCirJS library will allow the placement of elements now, but the question is now how to go about it programmatically. I realize that this could have potential issues with workspace size, so finding an efficient means of laying out the data to the user will be important.</p>

</div>