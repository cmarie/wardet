<!-- Navigation bar for the whole site -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-collapse">
                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="?page=home">Home</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="menu-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="?page=editor">Editor</a>
                </li>
                <li>
                    <a href="?page=blog">Progress Updates</a>
                </li>
                <li>
                    <a href="?page=resume">Resume</a>
                </li>
                <li>
                    <a href="?page=details">Project Description</a>
                </li>
                <li>
                    <a href="?page=philosophy">Philosophy Statement</a>
                </li>
                <li>
                    <?php if(!loggedIn()){ ?>
                    <a href="?page=login">Login</a>
                    <?php }else{ ?>
                        <a href="processor.php?action=logout">Logout</a>
                    <?php } ?>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>