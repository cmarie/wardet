<?php
    require "core/globals.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ethan Ward's Set Notation Visualizer</title>

    <!-- Bootstrap Core CSS -->
    <!-- Latest compiled and minified CSS -->
    <?php
        css(['bootstrap.min', 'font-awesome.min', 'simcir', 'simcir-basicset', 'style']);
    ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->

    <script>window.App = {user_id: <?php echo user('id') ? user('id') : 'null';?>}</script>
</head>

<body>
    <?php //adds js libraries and files to project
        element('main_menu');
        content_for_layout();
        js([
            'jquery.min',
            'bootstrap.min',
            'simcir',
            'simcir-basicset',
            'simcir-library',
            'FileSaver',
            'init',
            'trees'
        ]);
    ?>
</body>
</html>
