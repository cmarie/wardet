<?php
    element('editor_menu');
?>
<div id="editor-page">
    <div class="simcir" data-schematic-id="<?php echo(!empty($_GET['id']) ? $_GET['id'] : null); ?>"></div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="import-modal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="processor.php?action=import_schematic" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Import Wizard</h4>
                </div>
                <div class="modal-body">
                    <input type="file" name="data" id="data" accept=".json"/>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="help-modal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p>Choose a device from the toolbox and move to right side.</p>
                <p>Connect them by drag operation.</p>
                <p>Click an input node to disconnect.</p>
                <p>Move a device back to the toolbox if you don't use.</p>
                <p>Ctrl+Click(Mac:command+Click) to toggle view (Live circuit or JSON data).</p>
                <p>Double-Click a label to edit device name.</p>
                <p>Double-Click a library to open the circuit inside.</p>
            </div>
        </div>
    </div>
</div>