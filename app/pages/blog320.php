<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>My validation function is a mess. I go through the input string checking the if the coming character could follow the current character. Parentheses caused a bit of confusion because it required recursion. Ultimately I settled on traversing the string until the parens were balanced, then sending everything within the parens to a new parse call. I spent a long time attempting to find a more elegant solution than a for loop, like regex. I handle the last char in a string separately, and I handle single char strings separately.</p>
    
    <p>Parsing of user input is now appears functional, though I should probably recruit someone to test it to remove any partiality. The next step is generating a parse tree out of the valid input, so everything I had discussed about recursive descent parsing is relevant again as I had taken a different approach to validating the input.</p>

</div>