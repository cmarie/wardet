<div class="container">
    
    <h2>Ethan Ward's Project Blog</h2>
    
    <p>Progress has been made on a number of issues. The user input problem has been fixed. Apparently I must have made a typo, uploaded that typo to the server, corrected it in my local files and then never uploaded the change, so time was wasted on fixing something that shouldn’t have been broken. I also discovered the use of the console for debugging. I probably should have known about this for over a year, but now that I am using it, I have a far better understanding of what my issues are with my parsing. While there is still some odd behavior with some interactions, generally my current problems are what I feared. When I wrote the functions for this, I was always checking the character ahead until the second to last in order to avoid looking at indices past the size of my input string. Brilliantly, I was not actually accounting for what that last char in my strings was, so that explains a number of the odd results I have had.</p>
    
    <p>Going forward, I think I have a decent understanding on how to approach creating my parse trees, as we dealt with creating some in our 322 Lab 2. So far, my plan has been to break everything down into their smallest possible elements with parentheses. So something like AuBnC would become ((AuB)nC), indicating the order of operations. A,B, and C signify sets, u signifies union, and n signifies intersection.</p>

</div>